# Bit Rebels - Improved Product Recommendation

Saksham Verma
Ambuj Srivastava
K Vamsi Krishna
Ashis Kumar Pradhan


# Project Overview

This project deals with a efficient recommender engine which shall focus upon **user's browsing behaviour who are interested in buying laptops**.
Generally users who wish to buy any laptop have to surf the internet and scan through various e-commerce websites in order to make their
choice.

## Our Proposal

We aim to build a recommendation engine based on a varity of factors for every single user, such that whenever they visit dell.com, they
are offered a range of products which match their requirements.

## Solution Description
We have developed **an extension** which shall store the web browsing history of every user when they visit e-commerce sites in order  to purchase
laptops. This data is **stored in our node js server** and the data is scraped according to the various features that we have accommodated in our **ML
engine**. As soon as the Machine Learning engine is triggered we get a **predicted output as a label** and this label is matched with our database and
the corresponding recommendations are displayed on the same extension when the user visits dell.com.

## Technologies Used
<ul>
<li>HTML</li>
<li>NodeJS</li>
<li>Bootstrap</li>
<li>CSS </li>
<li>Advanced CSS</li>
<li>jQuery</li>
<li>JavaScript</li>
<li>Machine Learning</li>
</ul>

## Setup Installation

For python files:-
install nltk package using pip install nltk

For node files: -
NodeJS
mongoDB
## Instructions to run the the submitted code

1) Enable extension and sign up
2) Visit e-commerce websites.
3) Browse for laptops
4) Visit dell.com
5) Open extension to view the recommended products.